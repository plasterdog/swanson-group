<?php
/**
 * Template Name: Split Page
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package swanson
 */
get_header(); ?>
		<div class="big-background">
		<div id="page" class="hfeed site">
			<div id="content" class="site-content" >
			<div id="primary" class="full-content-area">
			<main id="main" class="full-site-main" role="main">
			<?php while ( have_posts() ) : the_post(); ?>
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<header class="entry-header">
			<h1><?php the_title(); ?></h1>	
			</header><!-- .entry-header -->
<!-- MAKING THE CONTENT RESTRICTED -->
<?php if( !rcp_is_active() ) : ?>
<h2>This is private content</h2>
<p> please login for access</p>
<hr/> 
<?php echo do_shortcode("[login_form]"); ?>
<?php endif; ?> <!--ends conditional for unvalidated user -->
<!--activates restriction -->

<?php if( rcp_is_active() ) : ?>
<!-- THE VELVET ROPE IS LIFTED -->

			<div class="entry-content">
				<?php the_content(); ?>
				<div class="left-side">
				<?php the_field('left_section'); ?>	
				<?php
					wp_link_pages( array(
						'before' => '<div class="page-links">' . __( 'Pages:', 'swanson' ),
						'after'  => '</div>',
					) );
				?>
			</div><!-- ends left side -->
			<div class="right-side">
				<?php the_field('right_section'); ?>
			</div><!-- ends right side-->
			</div><!-- .entry-content -->
			<?php edit_post_link( __( 'Edit', 'swanson' ), '<footer class="entry-footer"><span class="edit-link">', '</span></footer>' ); ?>

<?php endif; ?><!-- for RCP -->	

			</article><!-- #post-## -->
			<?php endwhile; // end of the loop. ?>
		</main><!-- #main -->
	</div><!-- #primary -->
<div class="clear" style="height:2em;"></div>
</div><!-- ENDS BIG BACKGROUND -->
<?php get_footer(); ?>
