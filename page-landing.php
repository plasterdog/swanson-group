<?php
/**
 * Template Name: Landing Page
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package swanson
 */
get_header(); ?>
<div class="big-background">
		<div id="page" class="hfeed site">
		<div id="content" class="site-content" >
		<div id="primary" class="full-content-area">
			<main id="main" class="full-site-main" role="main">
			<?php while ( have_posts() ) : the_post(); ?>
<div class="entry-content">
<!-- MAKING THE CONTENT RESTRICTED -->
<?php if( !rcp_is_active() ) : ?>
<h2>This is private content</h2>
<p> please login for access</p>
<hr/> 
<?php echo do_shortcode("[login_form]"); ?>
<?php endif; ?> <!--ends conditional for unvalidated user -->
<!--activates restriction -->

<?php if( rcp_is_active() ) : ?>
<!-- THE VELVET ROPE IS LIFTED -->			
			<div class="left_half_division">	
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<header class="entry-header">

		<?php if ( get_field( 'swanson_left_section_title' ) ): ?>
       	<h3 style="padding-bottom:.5em;"><?php the_field('swanson_left_section_title'); ?></h3>
        <?php endif; // end of if field_name logic ?>

			</header><!-- .entry-header -->
			<?php the_content(); ?>
			</div><!-- ends left half division -->

		<div class="right_half_division">

		<?php if ( get_field( 'swanson_right_section_title' ) ): ?>
       	<h3><?php the_field('swanson_right_section_title'); ?></h3>
        <?php endif; // end of if field_name logic ?>

			<?php
			// check if the repeater field has rows of data
			if( have_rows('swanson_right_section_faq') ): ?>
			<?php   // loop through the rows of data
			while ( have_rows('swanson_right_section_faq') ) : the_row(); ?>
			<button class="accordion">
			 <?php the_sub_field('swanson_faq_title');?>
			</button>
			<div class="panel fade-in quarter">
			<?php the_sub_field('swanson_faq_contents');?>
			</div>

			<?php    endwhile; ?>
			<?php else :
			// no rows found
			endif; ?>

		</div> <!-- ends right_half_division -->	
<?php endif; ?><!-- for RCP -->

			</div><!-- .entry-content -->

	<?php edit_post_link( __( 'Edit', 'swanson' ), '<footer class="entry-footer"><span class="edit-link">', '</span></footer>' ); ?>
</article><!-- #post-## -->
			<?php endwhile; // end of the loop. ?>
			<hr/>


			<?php if ( get_field( 'category_slug_name' ) ): ?>

<?php if( rcp_is_active() ) : ?>
<!-- THE VELVET ROPE IS LIFTED -->			
					<?php
					global $post;
					$args = array( 'numberposts' =>get_field('number_of_excerpts'), 'offset'=> 0, 'category_name' =>get_field('category_slug_name'), 'orderby' => 'post_date', 'order' => 'DSC');
					$myposts = get_posts( $args );
					foreach( $myposts as $post ) :	setup_postdata($post); ?>
							
			<?php if ( get_the_post_thumbnail( $post_id ) != '' ) { ?>
			

				<?php if (!empty($post->post_excerpt)) : ?>
			
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>  
			<div class="archive_left_picture">	
			<a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_post_thumbnail( 'medium' ); ?></a>
			</div><!-- ends left picture -->
				<div class="archive_right_text">
				<header class="entry-header">
				<h1 class="entry-title"><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></h1>	
				</header><!-- .entry-header -->	
				<?php the_excerpt(); ?>
				<p align="right" style="margin-bottom:.5em;"><a href="<?php the_permalink(); ?>" rel="bookmark">find out more</a></p>
				<?php else : ?>
			
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>  
			<div class="archive_left_picture">	
			<?php the_post_thumbnail( 'medium' ); ?>
			</div><!-- ends left picture -->
				<div class="archive_right_text">
				<header class="entry-header">
				<h1 class="entry-title"><?php the_title(); ?></h1>	
				</header><!-- .entry-header -->
				<?php the_content(); ?>
				<?php endif; ?>	
				</div><!-- ends right text -->
			</article><!-- #post-## -->
		    <div class="clear"><hr/></div>

			<?php   } else { ?>
			
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>  


				<?php if (!empty($post->post_excerpt)) : ?>
		<header class="entry-header">
		<h1 class="entry-title"><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></h1>	
		</header><!-- .entry-header -->		
				<?php the_excerpt(); ?>
				<p align="right" style="margin-bottom:.5em;"><a href="<?php the_permalink(); ?>" rel="bookmark">find out more</a></p>
				<?php else : ?>
		<header class="entry-header">
		<h1 class="entry-title"><?php the_title(); ?></h1>	
		</header><!-- .entry-header -->			
				<?php the_content(); ?>
				<?php endif; ?>	
			</article><!-- #post-## -->	
			<div class="clear"><hr/></div>
		 	<?php    } ?>

			<?php endforeach; ?>
<?php endif; ?><!-- for RCP -->

<?php else: // field_name returned false ?>	


<!--if no entry in field query will show posts unfiltered by category -->	
<?php if( rcp_is_active() ) : ?>
<!-- THE VELVET ROPE IS LIFTED -->	

			<?php if ( get_field( 'number_of_excerpts' ) ): ?>
			<?php 
			// the query
			$wpb_all_query = new WP_Query(array('post_type'=>'post', 'post_status'=>'publish', 'posts_per_page' =>get_field('number_of_excerpts'))); ?>

			<?php else: // field_name returned false ?>	
			<?php 
			// the query
			$wpb_all_query = new WP_Query(array('post_type'=>'post', 'post_status'=>'publish', 'posts_per_page'=>-1)); ?>
			<?php endif; // end of if field_name logic ?>	
			
				<!-- the loop -->
			<?php while ( $wpb_all_query->have_posts() ) : $wpb_all_query->the_post(); ?>
					
				<?php if ( get_the_post_thumbnail( $post_id ) != '' ) { ?>
				
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>  
				<div class="archive_left_picture">	
				<a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_post_thumbnail( 'medium' ); ?></a>
				</div><!-- ends left picture -->
					<div class="archive_right_text">
					<header class="entry-header">
					<h1 class="entry-title"><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></h1>	
					</header><!-- .entry-header -->
				<?php if (!empty($post->post_excerpt)) : ?>
				<?php the_excerpt(); ?>
				<p align="right" style="margin-bottom:.5em;"><a href="<?php the_permalink(); ?>" rel="bookmark">find out more</a></p>
				<?php else : ?>
				<?php the_content(); ?>
				<?php endif; ?>	
					</div><!-- ends right text -->
				</article><!-- #post-## -->
			    <div class="clear"><hr/></div>

				<?php   } else { ?>
				
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>  
					<header class="entry-header">
					<h1 class="entry-title"><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></h1>	
					</header><!-- .entry-header -->
				<?php if (!empty($post->post_excerpt)) : ?>
				<?php the_excerpt(); ?>
				<p align="right" style="margin-bottom:.5em;"><a href="<?php the_permalink(); ?>" rel="bookmark">find out more</a></p>
				<?php else : ?>
				<?php the_content(); ?>
				<?php endif; ?>	
				</article><!-- #post-## -->
				<div class="clear"><hr/></div>		
			 	<?php    } ?>

				<?php endwhile; ?>
				<!-- end of the loop -->
<?php endif; ?><!-- for RCP -->

<?php endif; // end of if field_name logic ?>	
<?php wp_reset_postdata(); ?>


		</main><!-- #main -->
	</div><!-- #primary -->
	<div class="clear" style="height:2em;"></div>
</div><!-- ENDS BIG BACKGROUND -->
<?php get_footer(); ?>
