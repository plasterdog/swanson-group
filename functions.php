<?php
/**
 * swanson functions and definitions
 *
 * @package swanson
 */
require_once( __DIR__ . '/inc/customizer-styles.php');
require_once( __DIR__ . '/inc/simplify-profiles.php');
require_once( __DIR__ . '/inc/widget-styling.php');
require_once( __DIR__ . '/inc/backend-experience.php');
require_once( __DIR__ . '/inc/global-fields.php');
//require_once( __DIR__ . '/inc/advanced-custom-fields.php');
//require_once( __DIR__ . '/inc/required-plugins.php');
/**
 * Set the content width based on the theme's design and stylesheet.
 */
if ( ! isset( $content_width ) ) {
	$content_width = 640; /* pixels */
}

if ( ! function_exists( 'swanson_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function swanson_setup() {

	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on swanson, use a find and replace
	 * to change 'swanson' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'swanson', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );
	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	//add_theme_support( 'post-thumbnails' );
	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => __( 'Primary Menu', 'swanson' ),
		'footer' => __( 'Footer Menu', 'swanson' ),
	) );
	// Setup the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'swanson_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );


	// Enable support for HTML5 markup.
	add_theme_support( 'html5', array(
		'comment-list',
		'search-form',
		'comment-form',
		'gallery',
	) );
}
endif; // swanson_setup
add_action( 'after_setup_theme', 'swanson_setup' );

//https://wordpress.stackexchange.com/questions/250349/how-to-remove-menus-section-from-wordpress-theme-customizer

function mytheme_customize_register( $wp_customize ) {
  //All our sections, settings, and controls will be added here

  $wp_customize->remove_section( 'title_tagline');
  $wp_customize->remove_section( 'background_image');
  $wp_customize->remove_panel( 'nav_menus');
  $wp_customize->remove_panel( 'widgets');
  $wp_customize->remove_section( 'static_front_page');
}
add_action( 'customize_register', 'mytheme_customize_register',50 );




/**
 * Register widgetized area and update sidebar with default widgets.
 */
function swanson_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'News Archive & Post Sidebar', 'swanson' ),
		'id'            => 'sidebar-1',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h1 class="widget-title">',
		'after_title'   => '</h1>'
	) );

		register_sidebar( array(
		'name'          => __( 'Clients Archive & Post Sidebar', 'swanson' ),
		'id'            => 'sidebar-2',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h1 class="widget-title">',
		'after_title'   => '</h1>'
	) );

        register_sidebar( array(
        'name'          => __( 'Regular Page Sidebar', 'swanson' ),
        'id'            => 'sidebar-3',
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget'  => '</aside>',
        'before_title'  => '<h1 class="widget-title">',
        'after_title'   => '</h1>'
    ) );

		register_sidebar( array(
		'name'          => __( 'Retailers Archive & Post Sidebar', 'swanson' ),
		'id'            => 'sidebar-4',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h1 class="widget-title">',
		'after_title'   => '</h1>'
	) ); 

		register_sidebar( array(
		'name'          => __( 'Product Archive & Post Sidebar', 'swanson' ),
		'id'            => 'sidebar-5',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h1 class="widget-title">',
		'after_title'   => '</h1>'
	) );        


 }
add_action( 'widgets_init', 'swanson_widgets_init' );

/**
 * Enqueue scripts and styles.
 */



function swanson_scripts() {

wp_enqueue_style( 'reset-styles', get_template_directory_uri() . '/css/reset-styles.css',false,'1.1','all');

	wp_enqueue_style( 'swanson-style', get_stylesheet_uri() );

	wp_enqueue_script( 'swanson-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20120206', true );

		wp_enqueue_script( 'swanson-accordion', get_template_directory_uri() . '/js/accordion.js', array(), '20120206', true );

	wp_enqueue_script( 'swanson-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20130115', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'swanson_scripts' );

/**
 * Implement the Custom Header feature. - JMC ACTIVATED
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';
//JMC => additional image size
function pw_add_image_sizes() {
    add_image_size( 'large-thumb', 300, 300, true );

}
add_action( 'init', 'pw_add_image_sizes' );
 
function pw_show_image_sizes($sizes) {
    $sizes['large-thumb'] = __( 'Custom Thumb', 'swanson' );

 
    return $sizes;
}
add_filter('image_size_names_choose', 'pw_show_image_sizes');



 /* JMC --* Enable support for Post Thumbnails	 */
	add_theme_support( 'post-thumbnails' );

/* JMC because it is required */
	add_theme_support( 'title-tag' );

//JMC remove anchor link from more tag

function remove_more_anchor($link) {
     $offset = strpos($link, '#more-');
     if ($offset) {
          $end = strpos($link, '"',$offset);
     }
     if ($end) {
          $link = substr_replace($link, '', $offset, $end-$offset);
     }
     return $link;
}
add_filter('the_content_more_link', 'remove_more_anchor');

//JMC-https://carriedils.com/add-editor-style/ <= will apply the stylesheet to the editor view

add_action( 'init', 'cd_add_editor_styles' );
/**
 * Apply theme's stylesheet to the visual editor.
 *
 * @uses add_editor_style() Links a stylesheet to visual editor
 * @uses get_stylesheet_uri() Returns URI of theme stylesheet
 */
function cd_add_editor_styles() {
 add_editor_style( get_stylesheet_uri() );
}
//JMC- https://www.advancedcustomfields.com/resources/how-to-hide-acf-menu-from-clients/
//add_filter('acf/settings/show_admin', '__return_false');

//https://revelationconcept.com/wordpress-rename-default-posts-news-something-else/

function revcon_change_post_label() {
    global $menu;
    global $submenu;
    $menu[5][0] = 'News';
    $submenu['edit.php'][5][0] = 'News';
    $submenu['edit.php'][10][0] = 'Add News';
    $submenu['edit.php'][16][0] = 'News Tags';
}
function revcon_change_post_object() {
    global $wp_post_types;
    $labels = &$wp_post_types['post']->labels;
    $labels->name = 'News';
    $labels->singular_name = 'News';
    $labels->add_new = 'Add News';
    $labels->add_new_item = 'Add News';
    $labels->edit_item = 'Edit News';
    $labels->new_item = 'News';
    $labels->view_item = 'View News';
    $labels->search_items = 'Search News';
    $labels->not_found = 'No News found';
    $labels->not_found_in_trash = 'No News found in Trash';
    $labels->all_items = 'All News';
    $labels->menu_name = 'News';
    $labels->name_admin_bar = 'News';
}
 
add_action( 'admin_menu', 'revcon_change_post_label' );
add_action( 'init', 'revcon_change_post_object' );

//https://thomasgriffin.io/how-to-include-custom-post-types-in-wordpress-search-results/
add_filter( 'pre_get_posts', 'tgm_io_cpt_search' );
/**
 * This function modifies the main WordPress query to include an array of 
 * post types instead of the default 'post' post type.
 *
 * @param object $query  The original query.
 * @return object $query The amended query.
 */
function tgm_io_cpt_search( $query ) {
	
    if ( $query->is_search ) {
	$query->set( 'post_type', array( 'post', 'client', 'productitem', 'retailer' ) );
    }
    
    return $query;
    
}

//https://wpabsolute.com/limit-access-to-wordpress-admin-dashboard/
// Block non-administrators from accessing the WordPress back-end
//function wpabsolute_block_users_backend() {
	//if ( is_admin() && ! current_user_can( 'administrator' ) && ! wp_doing_ajax() ) {
		//wp_redirect( home_url() );
		//exit;
	//}
//}
//add_action( 'init', 'wpabsolute_block_users_backend' );

//https://jeroensormani.com/block-dashboard-access-non-admins/
function js_hide_admin_bar( $show ) {
	if ( ! current_user_can( 'administrator' ) ) {
		return false;
	}
	return $show;
}
add_filter( 'show_admin_bar', 'js_hide_admin_bar' );