<?php
/**
 * The Template for displaying all single posts.
 *
 * @package swanson
 */

get_header(); ?>

<div class="big-background">

    <div id="page" class="hfeed site">
  <div id="content" class="site-content" >
  <div id="primary" class="content-area">
    <main id="main" class="site-main" role="main">

    <?php while ( have_posts() ) : the_post(); ?>

  <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
  <header class="entry-header">
  <div class="entry-meta"></div><!-- .entry-meta -->
  </header><!-- .entry-header -->
  <div class="entry-content">
  
<!-- MAKING THE CONTENT RESTRICTED -->
<?php if( !rcp_is_active() ) : ?>
 <h1><?php the_title(); ?></h1> 
<h2>This is private content</h2>
<p> please login for access</p>

<hr/> 
<?php echo do_shortcode("[login_form]"); ?>
<?php endif; ?> <!--ends conditional for unvalidated user --> 

<!--activates restriction -->

<?php if( rcp_is_active() ) : ?>
<!-- THE VELVET ROPE IS LIFTED -->
   

    <div class="archive_left_picture">  
        <?php if ( get_field( 'swanson_company_website_url' ) ): ?>
        <p><a href="<?php the_field('swanson_company_website_url'); ?>" target="_blank"><?php the_post_thumbnail( 'medium' ); ?></a></p>
        <?php else: // field_name returned false ?>
         <?php the_post_thumbnail( 'medium' ); ?> 
        <?php endif; // end of if field_name logic ?>
   </div><!-- ends left picture -->

    <div class="archive_right_text"><h1><?php the_title(); ?></h1>

        <?php if ( get_field( 'swanson_company_website_url' ) ): ?>
        <p><a href="<?php the_field('swanson_company_website_url'); ?>" target="_blank">Company Website Link</a></p>
        <?php else: // field_name returned false ?>
        <?php endif; // end of if field_name logic ?>

     </div><!-- ends right text -->   

<div class="clear" style="height:2em;"><hr/></div>
 <div class="clear">

        <?php
        /*
        *  Loop through post objects (assuming this is a multi-select field) ( setup postdata )
        *  Using this method, you can use all the normal WP functions as the $post object is temporarily initialized within the loop
        *  Read more: http://codex.wordpress.org/Template_Tags/get_posts#Reset_after_Postlists_with_offset
        */
        $post_objects = get_field('swanson_related_entity');
        if( $post_objects ): ?>
          
          <h3>Connections:</h3>
            <ul class="related-company">
            <?php foreach( $post_objects as $post): // variable must be called $post (IMPORTANT) ?>
                <?php setup_postdata($post); ?>
                <li>
                    <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
        <!--https://wordpress.stackexchange.com/questions/169504/how-to-get-current-get-post-types-name-->            
        (<?php 
        $postType = get_post_type_object(get_post_type());
        if ($postType) {
            echo esc_html($postType->labels->singular_name);
        }
         ?>)
                </li>
            <?php endforeach; ?>
            </ul>
            <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
            <div class="clear" style="height:2em;"></div><hr/>
        <?php endif;

        ?>
  </div> <!-- ends clear -->


    
    <div class="left_half_division">
      <?php
      // check if the repeater field has rows of data
      if( have_rows('swanson_relevant_files') ): ?>
      <h3>Relevant Files:</h3>
      <ul>
        <?php   // loop through the rows of data
        while ( have_rows('swanson_relevant_files') ) : the_row(); ?>
            <li><a href="<?php the_sub_field('swanson_linked_file');?>" target="_blank"><?php the_sub_field('swanson_linked_file_label');?></a></li>
        <?php    endwhile; ?>
      </ul>
      <?php else :
      // no rows found
      endif; ?>
    </div> <!-- ends left_half_division -->

    <div class="right_half_division">
      <?php
      // check if the repeater field has rows of data
      if( have_rows('swanson_relevant_urls') ): ?>
      <h3>Relevant Links:</h3>
      <ul>
        <?php   // loop through the rows of data
        while ( have_rows('swanson_relevant_urls') ) : the_row(); ?>
            <li><a href="<?php the_sub_field('swanson_link_target');?>" target="_blank"><?php the_sub_field('swanson_link_label');?></a></li>
        <?php    endwhile; ?>
      </ul>
      <?php else :
      // no rows found
      endif; ?>
    </div> <!-- ends right_half_division -->
<div class="clear" style="height:2em;"><hr/></div>

<div class="left_half_division">    
  <h3>Additional Notes:</h3>
  <?php the_content(); ?></div> <!-- ends left_half_division -->
<div class="right_half_division">


<?php
// check if the repeater field has rows of data
if( have_rows('swanson_contacts') ): ?>
<h3>Contacts:</h3>
<?php   // loop through the rows of data
while ( have_rows('swanson_contacts') ) : the_row(); ?>
<button class="accordion">
 <?php the_sub_field('swanson_contact_name');?>
</button>
<div class="panel fade-in quarter">
<p><strong>Title:</strong>&nbsp;<?php the_sub_field('swanson_contact_title');?></p>
<p><strong>Phone:</strong>&nbsp;<?php the_sub_field('swanson_contact_phone');?></p>
<p><strong>Email:</strong>&nbsp;<?php the_sub_field('swanson_contact_email');?></p>
<p><strong>Location:</strong></p><?php the_sub_field('swanson_contact_location');?>
</div>

<?php    endwhile; ?>
<?php else :
// no rows found
endif; ?>

</div> <!-- ends right_half_division -->  


<?php the_tags( __( 'Tags: ', 'swanson' ), ' ', '' ); ?>

<?php endif; ?><!-- for RCP -->
  </div><!-- .entry-content -->

  <footer class="entry-footer">
    
    <?php edit_post_link( __( 'Edit', 'swanson' ), '<span class="edit-link">', '</span>' ); ?>
  </footer><!-- .entry-footer -->
</article><!-- #post-## -->

        <div class="clear"><!-- variation from default nav which restricts navigation within category -->
<div class="left-split-nav"><?php //previous_post_link('%link', '&larr; %title', TRUE) ?></div>
<div class="right-split-nav"><?php //next_post_link('%link', '%title &rarr;', TRUE) ?></div>
</div>

      <?php
        // If comments are open or we have at least one comment, load up the comment template
        if ( comments_open() || '0' != get_comments_number() ) :
          comments_template();
        endif;
      ?>

    <?php endwhile; // end of the loop. ?>

    </main><!-- #main -->
  </div><!-- #primary -->

  <div id="secondary" class="widget-area" role="complementary">
<!-- MAKING THE CONTENT RESTRICTED -->
<?php if( !rcp_is_active() ) : ?>
<?php endif; ?> <!--ends conditional for unvalidated user --> 
<!--activates restriction -->
<?php if( rcp_is_active() ) : ?>
<!-- THE VELVET ROPE IS LIFTED -->

     <?php if ( ! dynamic_sidebar( 'sidebar-4' ) ) : ?>
    <?php endif; // end sidebar widget area ?>

<?php endif; ?><!-- for RCP -->

  </div><!-- #secondary -->


  <div class="clear" style="height:2em;"></div>
</div><!-- ENDS BIG BACKGROUND -->
<?php get_footer(); ?>