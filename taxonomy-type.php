<?php
/**
 * The template for displaying Market Archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package swanson
 */

get_header(); ?>
		<div class="big-background">
		<div id="page" class="hfeed site">
		<div id="content" class="site-content" >
		<section id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
		<header class="page-header">
				<h1 class="page-title">
					<?php single_cat_title();?>
				</h1>

<!-- MAKING THE CONTENT RESTRICTED -->
<?php if( !rcp_is_active() ) : ?>
<h2>This is private content</h2>
<p> please login for access</p>
<hr/>	
<?php echo do_shortcode("[login_form]"); ?>
<?php endif; ?> <!--ends conditional for unvalidated user -->
<!--activates restriction -->

<?php if( rcp_is_active() ) : ?>
<!-- THE VELVET ROPE IS LIFTED -->

				<?php
					// Show an optional term description.
					$term_description = term_description();
					if ( ! empty( $term_description ) ) :
						printf( '<div class="taxonomy-description">%s</div>', $term_description );?>
			<?php endif;?>
			<hr/>
		</header>
		<?php if ( have_posts() ) : ?>
			<?php /* Start the Loop */ ?>
			<ul class="company-icon-array">
			<?php global $query_string;
			query_posts( $query_string.'&order=ASC&orderby=name' );	?>
			<?php while ( have_posts() ) : the_post(); ?>
			<!-- POST FIELDS-->
			<li>
			<a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_post_thumbnail( 'medium' ); ?></a>
			<h3><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></h3>	
			</li>
		<?php if ( ! post_password_required() && ( comments_open() || '0' != get_comments_number() ) ) : ?>
	<?php endif; ?>
<!-- ENDS POST FIELDS -->
			<?php endwhile; ?>
</ul><!-- ends archive array-->
			<?php swanson_paging_nav(); ?>
		<?php else : ?>
			<?php get_template_part( 'content', 'none' ); ?>
		<?php endif; ?>

<?php endif; ?><!-- for RCP -->		
		</main><!-- #main -->
	</section><!-- #primary -->
	<div id="secondary" class="widget-area front-book-array" role="complementary">

<!-- MAKING THE CONTENT RESTRICTED -->
<?php if( !rcp_is_active() ) : ?>
<?php endif; ?> <!--ends conditional for unvalidated user --> 
<!--activates restriction -->
<?php if( rcp_is_active() ) : ?>
<!-- THE VELVET ROPE IS LIFTED -->

			<?php if ( ! dynamic_sidebar( 'sidebar-5' ) ) : ?>
			<?php endif; // end sidebar widget area ?>

<?php endif; ?><!-- for RCP -->

	</div><!-- #secondary -->
	<div class="clear" style="height:2em;"></div>
</div><!-- ENDS BIG BACKGROUND -->
<?php get_footer(); ?>
