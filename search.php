<?php
/**
 * The template for displaying Search Results pages.
 *
 * @package swanson
 */

get_header(); ?>

		<div class="clear"></div>
<div class="big-background">
		<div id="page" class="hfeed site">
	<div id="content" class="site-content" >
	<section id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php if ( have_posts() ) : ?>

			<header class="page-header">
				<h1 class="page-title"><?php printf( __( 'Search Results for: %s', 'swanson' ), '<span>' . get_search_query() . '</span>' ); ?></h1>
			</header><!-- .page-header -->

			<?php /* Start the Loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>
					<ul class="company-icon-array">
					<li>
					<a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_post_thumbnail( 'medium' ); ?></a>	
					<h3><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></h3>	
					</li>
						<?php if ( ! post_password_required() && ( comments_open() || '0' != get_comments_number() ) ) : ?>
						
						<?php endif; ?>
					</ul>
			<?php endwhile; ?>
			<?php swanson_paging_nav(); ?>
		<?php else : ?>
			<?php get_template_part( 'content', 'none' ); ?>
<div class="clear"><hr/></div>
		<?php endif; ?>

		</main><!-- #main -->
	</section><!-- #primary -->
<!-- MAKING THE CONTENT RESTRICTED -->
<?php if( !rcp_is_active() ) : ?>
<?php endif; ?> <!--ends conditional for unvalidated user --> 
<!--activates restriction -->
<?php if( rcp_is_active() ) : ?>
<!-- THE VELVET ROPE IS LIFTED -->

<?php get_sidebar(); ?>

<?php endif; ?><!-- for RCP -->

	<div class="clear" style="height:2em;"></div>
</div><!-- ENDS BIG BACKGROUND -->
<?php get_footer(); ?>
