<?php
/**
 * The template for displaying 404 pages (Not Found).
 *
 * @package swanson
 */

get_header(); ?>

		<div class="clear"></div>
<div class="big-background">
		<div id="page" class="hfeed site">
	<div id="content" class="site-content" >
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<section class="error-404 not-found">
				<header class="page-header">
					<h1 class="page-title"><?php _e( 'Oops! That page can&rsquo;t be found.', 'swanson' ); ?></h1>
				</header><!-- .page-header -->

				<div class="page-content">
					<p><?php _e( 'It looks like nothing was found at this location. Maybe try one of the links below or a search?', 'swanson' ); ?></p>

					<?php get_search_form(); ?>


					<?php
					/* translators: %1$s: smiley */
					$archive_content = '<p>' . sprintf( __( 'Try looking in the monthly archives. %1$s', 'swanson' ), convert_smilies( ':)' ) ) . '</p>';
					the_widget( 'WP_Widget_Archives', 'dropdown=1', "after_title=</h2>$archive_content" );
					?>

					<?php the_widget( 'WP_Widget_Tag_Cloud' ); ?>

				</div><!-- .page-content -->
			</section><!-- .error-404 -->

		</main><!-- #main -->
	</div><!-- #primary -->
<!-- MAKING THE CONTENT RESTRICTED -->
<?php if( !rcp_is_active() ) : ?>
<?php endif; ?> <!--ends conditional for unvalidated user --> 
<!--activates restriction -->
<?php if( rcp_is_active() ) : ?>
<!-- THE VELVET ROPE IS LIFTED -->

<?php get_sidebar(); ?>


<?php endif; ?><!-- for RCP -->

	<div class="clear" style="height:2em;"></div>
</div><!-- ENDS BIG BACKGROUND -->
<?php get_footer(); ?>