<?php
/**
 * The Template for displaying all single posts.
 *
 * @package swanson
 */

get_header(); ?>

<div class="big-background">

    <div id="page" class="hfeed site">
  <div id="content" class="site-content" >
  <div id="primary" class="content-area">
    <main id="main" class="site-main" role="main">

    <?php while ( have_posts() ) : the_post(); ?>

  <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
  <header class="entry-header">  <div class="entry-meta"></div><!-- .entry-meta -->  </header><!-- .entry-header -->
  <div class="entry-content">
    <h1><?php the_title(); ?></h1>
<!-- MAKING THE CONTENT RESTRICTED -->
<?php if( !rcp_is_active() ) : ?>
<h2>This is private content</h2>
<p> please login for access</p>
<hr/> 
<?php echo do_shortcode("[login_form]"); ?>
<?php endif; ?> <!--ends conditional for unvalidated user -->
<!--activates restriction -->

<?php if( rcp_is_active() ) : ?>
<!-- THE VELVET ROPE IS LIFTED -->

    <?php the_content(); ?>

<?php the_tags( __( 'Tags: ', 'swanson' ), ' ', '' ); ?>

<?php endif; ?><!-- for RCP -->
  </div><!-- .entry-content -->

  <footer class="entry-footer">
    
    <?php edit_post_link( __( 'Edit', 'swanson' ), '<span class="edit-link">', '</span>' ); ?>
  </footer><!-- .entry-footer -->
</article><!-- #post-## -->

        <div class="clear"><!-- variation from default nav which restricts navigation within category -->
<div class="left-split-nav"><?php //previous_post_link('%link', '&larr; %title', TRUE) ?></div>
<div class="right-split-nav"><?php //next_post_link('%link', '%title &rarr;', TRUE) ?></div>
</div>

      <?php
        // If comments are open or we have at least one comment, load up the comment template
        if ( comments_open() || '0' != get_comments_number() ) :
          comments_template();
        endif;
      ?>

    <?php endwhile; // end of the loop. ?>

    </main><!-- #main -->
  </div><!-- #primary -->

  <div id="secondary" class="widget-area" role="complementary">

<!-- MAKING THE CONTENT RESTRICTED -->
<?php if( !rcp_is_active() ) : ?>
<?php endif; ?> <!--ends conditional for unvalidated user --> 
<!--activates restriction -->
<?php if( rcp_is_active() ) : ?>
<!-- THE VELVET ROPE IS LIFTED -->

     <?php if ( ! dynamic_sidebar( 'sidebar-1' ) ) : ?>
    <?php endif; // end sidebar widget area ?>

<?php endif; ?><!-- for RCP -->

  </div><!-- #secondary -->
  <div class="clear" style="height:2em;"></div>
</div><!-- ENDS BIG BACKGROUND -->
<?php get_footer(); ?>